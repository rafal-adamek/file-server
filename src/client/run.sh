#!/bin/sh

kotlinc Client.kt ClientRunner.kt -include-runtime -d Client.jar
java -jar Client.jar

#kotlinc Client.kt ClientRunner.kt -cp kotlinx-coroutines-core-1.3.5.jar -include-runtime -d Client.jar
#java -cp kotlinx-coroutines-core-1.3.5.jar:Client.jar
