package client

import java.io.File

fun main() {
    val path = "files"
    val timeInterval = 30L

    val fileSynchronizer = Runnable {
        while (true) {
            File(path).listFiles()?.forEach {
                with(Client()) {
                    println("Starting client: sending ${it.path}")
                    sendFile(it.path)
                    println("Done")
                }
            }
            Thread.sleep(timeInterval.secondsToMillis())
        }
    }

    Thread(fileSynchronizer).start()
}

fun Long.secondsToMillis() = this * 1000
