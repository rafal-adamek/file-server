package client

import java.io.DataOutputStream
import java.io.File
import java.io.FileInputStream
import java.net.Socket

/**
 * Simple client that sends files through a [Socket]
 *
 * Server excepts all kinds of files with the following contract:
 * - First long defines file size
 * - Next string defines filename with an extension
 * - Rest is the file data
 *
 * @param host of the server
 * @param port of the server
 */
class Client(
    host: String = HOSTNAME,
    port: Int = PORT
) {

    private val socket: Socket = Socket(host, port)
    private val buffer = ByteArray(BUFFER_SIZE)

    fun sendFile(filename: String) {

        val (dataOutStream, fileInStream) = DataOutputStream(socket.getOutputStream()) to FileInputStream(filename)

        with(dataOutStream) {
            writeLong(File(filename).length())
            writeUTF(File(filename).name)
            while (fileInStream.read(buffer) > 0) {
                write(buffer)
            }
            close()
        }

        fileInStream.close()
    }

    companion object {
        const val HOSTNAME = "localhost"
        const val PORT = 4444
        const val BUFFER_SIZE = 4096
    }

}
