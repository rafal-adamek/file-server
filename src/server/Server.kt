package server

import java.io.DataInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.ServerSocket
import java.net.Socket
import kotlin.math.min

/**
 * Runs infinitely until user cancels it manually.
 *
 * Server excepts all kinds of files with the following contract:
 * - First long defines file size
 * - Next string defines filename with an extension
 * - Rest is the file data
 *
 * @param port to listen on
 * @param dirPath where to store files
 * @param buffer used in stream operations
 */
class Server(
    port: Int = PORT,
    private val dirPath: String = "files",
    private val buffer: ByteArray = ByteArray(BUFFER_SIZE)
) {

    init {
        newDir(dirPath)
    }

    private val socket = ServerSocket(port)

    fun run() {
        while (true) saveFile(socket.accept())
    }

    /**
     * Saves file in a previously specified path
     * @param socket with incoming data
     */
    private fun saveFile(socket: Socket) {
        val dataInStream = DataInputStream(socket.getInputStream())

        val (size, name) = dataInStream.readLong().toInt() to dataInStream.readUTF()
        var (total, remaining) = 0 to size

        val fileOutStream = FileOutputStream(File("$dirPath/$name").also { it.createNewFile() })

        do {
            val read = dataInStream.read(buffer, 0, min(buffer.size, remaining))
            total += read
            remaining -= read
            println("read $total bytes.")
            fileOutStream.write(buffer, 0, read)
        } while (read > 0)

        fileOutStream.close()
        dataInStream.close()
    }

    /**
     * Creates new directory if it doesn't exist
     * @param path to directory
     */
    private fun newDir(path: String) =
        with(File(path)) {
            if(!exists()) mkdirs()
        }

    companion object {
        const val PORT = 4444
        const val BUFFER_SIZE = 4096
    }
}
