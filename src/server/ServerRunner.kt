package server

fun main() {
    println("Starting server")
    with(Server()) {
        println("Listening...")
        run()
    }
}
